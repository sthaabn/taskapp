import React, { Component } from "react";
import { StyleSheet, Text, View, Button } from "react-native";
import {
  createStackNavigator,
  createAppContainer,
  createDrawerNavigator,
  DrawerNavigator
} from "react-navigation";

import Footer from "./components/Footer";
import Images from "./components/Images";
import Buttons from "./components/Buttons";
import Login from "./components/Login";
import Signup from "./components/Signup";
import Menu from "./components/Menu";
function App(props) {
  toggleDrawer = () => {
    props.navigationProps.toggleDrawer();
  };
  return (
    <View style={styles.container}>
      <View>
        <Images />
      </View>
      <View>
        <Buttons navigate={props.navigation.navigate} />
      </View>
      <View>
        <Footer />
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: "column",
    backgroundColor: "#246AC7"
  }
});

const RootStack = createStackNavigator(
  {
    App,
    Login,
    Signup,
    Menu
  },
  {
    initalRouteName: "App"
  }
);

const AppContainer = createAppContainer(RootStack);

export default AppContainer;
