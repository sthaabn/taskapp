import React, { Component } from "react";
import {
  StyleSheet,
  Text,
  Image,
  View,
  TouchableOpacity,
  TextInput
} from "react-native";

export class Signup extends Component {
  render() {
    return (
      <View>
        <Image style={styles.image} source={require("../assets/logo.png")} />
        <Text style={styles.titleText}>Create Account</Text>
        <Text style={styles.text}>What's your name?</Text>
        <TextInput style={styles.textInput} />

        <Text style={styles.text}>What's your email?</Text>
        <TextInput style={styles.textInput} />
        <Text styles={styles.holder}>
          {" "}
          You will need to confirm this email later.
        </Text>
        <Text style={styles.text}>Pick a password</Text>

        <TextInput style={styles.textInput} secureTextEntry={true} />
        <Text style={styles.holder}>Use at least eight characters</Text>
        <Text style={styles.text}>What's your gender?</Text>
        <TextInput style={styles.textInput} />
        <TouchableOpacity style={styles.button}>
          <Text style={styles.buttonStyle}>SIGN UP</Text>
        </TouchableOpacity>
      </View>
    );
  }
}
Signup.navigationOptions = {};

const styles = StyleSheet.create({
  image: {
    marginLeft: 250
  },
  titleText: {
    color: "#017AFF",
    fontSize: 30,
    marginLeft: 12,
    marginRight: 12,
    marginBottom: 5
  },
  text: {
    color: "#017AFF",
    fontSize: 20,
    marginLeft: 12,
    marginRight: 12,
    marginTop: 10
  },
  textInput: {
    height: 40,
    borderColor: "#017AFF",
    borderWidth: 1,
    borderRadius: 5,
    marginLeft: 10,
    marginRight: 10,
    marginTop: 10,
    marginBottom: 10,
    backgroundColor: "#F6FCFF"
  },
  holder: {
    marginLeft: 10
  },
  button: {
    marginTop: 20,
    backgroundColor: "#017AFF"
  },
  buttonStyle: {
    textAlign: "center",
    color: "#fff",
    height: 50,
    padding: 15
  }
});

export default Signup;
