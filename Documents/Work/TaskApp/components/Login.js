import React, { Component } from "react";
import {
  StyleSheet,
  Text,
  Image,
  TextInput,
  TouchableOpacity,
  View,
  Button
} from "react-native";

export class Login extends Component {
  render() {
    return (
      <View>
        <Image style={styles.image} source={require("../assets/logo.png")} />
        <Text style={styles.titleText}>Login</Text>

        <Text style={styles.text}>Username</Text>
        <TextInput style={styles.textInput} />
        <Text style={styles.text}>Password</Text>
        <TextInput style={styles.textInput} secureTextEntry={true} />
        <TouchableOpacity>
          <Text style={styles.textPassword}> Forgot Password?</Text>
        </TouchableOpacity>

        <TouchableOpacity
          onPress={() => this.props.navigation.navigate("Menu")}
          style={styles.button}
        >
          <Text style={styles.buttonStyle}>LOGIN</Text>
        </TouchableOpacity>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  image: {
    marginLeft: 250
  },
  titleText: {
    color: "#017AFF",
    fontSize: 30,
    marginLeft: 12,
    marginRight: 12,
    marginBottom: 20
  },
  text: {
    color: "#017AFF",
    fontSize: 20,
    marginLeft: 12,
    marginRight: 12,
    marginTop: 10
  },
  textInput: {
    height: 40,
    borderColor: "#017AFF",
    borderWidth: 1,
    borderRadius: 5,
    marginLeft: 10,
    marginRight: 10,
    marginTop: 10,
    marginBottom: 10,
    backgroundColor: "#F6FCFF"
  },
  textPassword: {
    textAlign: "center",
    marginTop: 20,
    color: "#017AFF",
    fontSize: 20
  },
  button: {
    marginTop: 20,
    backgroundColor: "#017AFF"
  },
  buttonStyle: {
    textAlign: "center",
    color: "#fff",
    height: 50,
    padding: 15
  }
});
export default Login;
