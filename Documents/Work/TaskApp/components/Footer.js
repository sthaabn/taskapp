import React, { Component } from "react";
import { StyleSheet, Text, View, Button, TouchableOpacity } from "react-native";

class Footer extends Component {
  render() {
    return (
      <View style={styles.footer}>
        <TouchableOpacity>
          <Text style={styles.text}>Login with Gmail</Text>
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  footer: {},
  text: {
    color: "#017AFF",
    fontSize: 20,
    padding: 20,
    width: "100%",
    textAlign: "center",
    backgroundColor: "#fff"
  }
});

export default Footer;
