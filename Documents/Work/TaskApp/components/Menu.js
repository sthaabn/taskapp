import React, { Component } from "react";
import {
  StyleSheet,
  Text,
  Image,
  TextInput,
  TouchableOpacity,
  View,
  TableView,
  Item,
  Button
} from "react-native";
export class Menu extends Component {
  render() {
    return (
      <View>
        <View style={styles.top}>
          <Image style={styles.image} source={require("../assets/logo.png")} />
          <Text style={styles.title}>My Book</Text>
        </View>
        <View>
          <Text>Dashboard</Text>
          <Text>Journal</Text>
          <Text>General Ledger</Text>
          <Text>Trial Balance</Text>
          <Text>Profile</Text>
          <Text>Accounts</Text>
          <Text>Settings</Text>
          <Text>About Us</Text>
        </View>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  top: {
    flexDirection: "row",
    margin: 20
  },
  image: {},
  title: {
    marginTop: 20,
    marginLeft: 20,
    fontSize: 20,
    fontWeight: "bold",
    color: "#E88727"
  }
});

export default Menu;
