import React, { Component } from "react";
import { StyleSheet, Text, View, Image, Button } from "react-native";

class Images extends Component {
  render() {
    return (
      <View style={styles.footer}>
        <Image source={require("../assets/logo.png")} />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  footer: {
    height: "80%",

    color: "#fff",
    alignItems: "center",
    justifyContent: "center"
  }
});

export default Images;
