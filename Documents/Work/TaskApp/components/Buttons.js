import React, { Component } from "react";
import { StyleSheet, View, Button, TouchableOpacity, Text } from "react-native";

export default function Buttons(props) {
  return (
    <View style={styles.login}>
      <TouchableOpacity
        onPress={() => props.navigate("Signup")}
        style={styles.buttonBorder}
      >
        <Text style={styles.text}>SIGNUP</Text>
      </TouchableOpacity>
      <TouchableOpacity
        onPress={() => props.navigate("Login")}
        style={styles.buttonBorder}
      >
        <Text style={styles.text}>LOGIN</Text>
      </TouchableOpacity>
    </View>
  );
}

const styles = StyleSheet.create({
  buttonBorder: {
    borderWidth: 1,
    borderColor: "white"
  },
  login: {
    flexDirection: "row"
  },
  text: {
    color: "#fff",
    padding: 10,
    marginLeft: 80,
    marginRight: 50,
    marginBottom: 50,
    // textAlign:'center',
    borderTopWidth: 2,
    borderColor: "#fff",
    fontSize: 20
  }
});
